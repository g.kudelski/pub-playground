# CSV Importer code challenge program

Simple java program to import CSV files into database.

## Compilation
Minimal external dependencies are required - only relevant JDBC DB driver in runtime and jUnit 4 for test compilation and execution. Java version 8 required.

Example:

    javac -cp .:/Applications/IntelliJ\ IDEA.app/Contents/lib/junit-4.12.jar  *.java


## Usage
As DB engine is configurable DB driver is not included.
Default configuration assumes Derby DB therefore it's driver needs to be present on classpath. It is part of JDK.

### Basic invocation
    java CsvImporter

This will run importer against default Derby DB and import all `*.csv` files beneath current directory using default delimiter (";") and accumulating up to 10 rows for single INSERT statement.


### Command arguments
Program accepts multiple CSV files or paths to scan for those as parameters. All resolved files will be imported.

`--debug` flag enables more verbose output

## Configuration

Configuration is done using following system environment variables:

* `CSV_IMP_DELIM` for delimiter used in CSV files
* `CSV_IMP_ROW_COUNT` sets count of rows accumulated in single INSERT statement
* `CSV_IMP_DB_URI` allows custom DB configuration, accepts standard JDBC URI

## Tests

Application logic tests are included. To run from command line invoke with jUnit on classpath:

    java org.junit.runner.JUnitCore CsvParserTest

## Advanced invocation examples

Multiple input files with debug enabled

    java CsvImporter --debug file1.csv file2.csv

Runtime classpath definition for DB driver

    java -cp /Library/Java/JavaVirtualMachines/jdk1.8.0_152.jdk/Contents/Home/db/lib/derby.jar:. CsvImporter

Runtime configuration for local MySQL db with runtime classpath

    CSV_IMP_DB_URI='jdbc:mysql:///csvdb?user=csv&password=[pwd]&useSSL=false' java -cp lib/mysql-connector-java-5.1.45.jar:. CsvImporter

Runtime configuration with custom value for single insert row count

    CSV_IMP_ROW_COUNT=3 java CsvImporter
