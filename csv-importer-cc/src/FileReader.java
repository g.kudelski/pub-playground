import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Generic file reader with header line handling
 *
 * @param <T> line consumer class
 */
class FileReader<T extends AutoCloseable> {
    private static final SimpleLoggerStub LOG = SimpleLoggerStub.getLogger(FileReader.class);
    private static final Function<Path, String> plainFileNameExtractor = p -> p.getFileName()
                                                                               .toString()
                                                                               .replaceFirst("\\.\\w{3}$", "")
                                                                               .replace(" ", "_")
                                                                               .replace(".", "_");

    private final Function<String, T> fileNameAwareConsumerFactory;
    private final Function<T, Consumer<String>> headerLineConsumerMapping;
    private final Function<T, Consumer<String>> dataLineConsumerMapping;

    FileReader(final Function<String, T> fileNameAwareConsumerFactory,
               final Function<T, Consumer<String>> headerLineConsumerMapping,
               final Function<T, Consumer<String>> dataLineConsumerMapping) {
        this.fileNameAwareConsumerFactory = fileNameAwareConsumerFactory;
        this.headerLineConsumerMapping = headerLineConsumerMapping;
        this.dataLineConsumerMapping = dataLineConsumerMapping;
    }

    /**
     * Reads all lines from file with separate handling for first (header) line
     *
     * @param file               to be read
     * @param headerLineConsumer first (header) line consumer
     * @param dataLineConsumer   other (data) lines consumer
     * @return number of lines read
     */
    int readFile(final Path file, final Consumer<String> headerLineConsumer, final Consumer<String> dataLineConsumer) {
        try (final Stream<String> csvFileLines = Files.lines(file)) {
            final int linesImported = csvFileLines.reduce(0, (lineCounter, line) -> {
                (lineCounter == 0 ? headerLineConsumer : dataLineConsumer).accept(line);
                return lineCounter + 1;
            }, Integer::sum);
            LOG.debug("Successfully read {} lines from '{}' file", linesImported, file.getFileName());
            return linesImported;
        } catch (IOException ex) {
            LOG.error("IO Exception while reading file '{}': {}", file.getFileName(), ex.getMessage());
            throw new RuntimeException();
        }
    }

    /**
     * Reads all submitted files
     *
     * @param files stream of source files
     * @return total number of lines read from all files
     */
    int readFiles(final Stream<Path> files) {
        return files.mapToInt(f -> {
            int linesImported = 0;
            try (final T consumer = fileNameAwareConsumerFactory.apply(plainFileNameExtractor.apply(f))) {
                linesImported = readFile(f, headerLineConsumerMapping.apply(consumer), dataLineConsumerMapping.apply(consumer));
            } catch (Exception ex) {
                LOG.error("Exception when handling file '{}': {}", f.getFileName(), ex.getMessage());
            }
            return linesImported;
        }).sum();
    }
}
