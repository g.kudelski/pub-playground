import java.util.ArrayList;
import java.util.List;

/**
 * DB update statement executor with data lines accumulation
 */
class AccumulatingStatementExecutor implements AutoCloseable {
    private static final SimpleLoggerStub LOG = SimpleLoggerStub.getLogger(AccumulatingStatementExecutor.class);
    private int insertedRowsTotal = 0;
    private CsvParser csvParser;
    private String tableName;
    private int accumulatedInsertRowsCount;
    private DbInstance dbInstance;
    private List<String> dataRowsBuffer = new ArrayList<>();

    AccumulatingStatementExecutor(final DbInstance dbInstance, final CsvParser csvParser,
                                  final String tableName, final int accumulatedInsertRowsCount) {
        this.dbInstance = dbInstance;
        this.csvParser = csvParser;
        this.tableName = tableName;
        this.accumulatedInsertRowsCount = accumulatedInsertRowsCount;
    }

    /**
     * Converts and immediately executes header line (table definition) statement
     *
     * @param line CSV header line
     */
    void processHeaderLine(final String line) {
        final String createStatement = csvParser.getCreateStatement(line, tableName);
        LOG.debug("Genereated create statement: {}", createStatement);
        final int rowsAffected = dbInstance.executeUpdate(createStatement);
        if (rowsAffected >= 0) {
            LOG.info("Successfully executed 'CREATE TABLE {}...' statement", tableName);
        }
    }

    /**
     * Accumulates data line and initiates processing of buffered data if accumulation limit is reached
     *
     * @param row CSV data line
     */
    void processDataLine(final String row) {
        dataRowsBuffer.add(row);
        LOG.debug("Collecting data rows {}/{}", dataRowsBuffer.size(), accumulatedInsertRowsCount);
        if (dataRowsBuffer.size() >= accumulatedInsertRowsCount) {
            processBufferedDataRows();
        }
    }

    /**
     * Processes accumulated data rows and executes insert statement
     */
    private void processBufferedDataRows() {
        csvParser.getInsertStatement(dataRowsBuffer.stream(), tableName).ifPresent(s -> {
            LOG.debug("Generated insert statement for {} lines: {}", dataRowsBuffer.size(), s);
            final int rowsAffected = dbInstance.executeUpdate(s);
            insertedRowsTotal += rowsAffected;
            LOG.debug("Successfully inserted {} data rows", rowsAffected);
            dataRowsBuffer.clear();
        });
    }

    /**
     * Flushes accumulation buffer on data end
     */
    @Override
    public void close() {
        processBufferedDataRows();
        LOG.info("Inserted {} data rows to '{}' table", insertedRowsTotal, tableName);
    }
}
