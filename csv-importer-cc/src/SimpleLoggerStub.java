import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Simple logger stub to avoid dependencies, debug flag program-wide
 */
class SimpleLoggerStub {
    private static boolean debugEnabled = false;
    private String className;
    private Function<String, BiFunction<String, Object[], String>> logLineBuilderProvider =
            lvl -> (msg, args) -> lvl + " [" + className + "]: " + replacePlaceholders(msg, args);

    private SimpleLoggerStub(final String className) {
        this.className = className;
    }

    static SimpleLoggerStub getLogger(final Class clazz) {
        return new SimpleLoggerStub(clazz.getName());
    }

    void setDebugEnabled(final boolean debugEnabled) {
        SimpleLoggerStub.debugEnabled = debugEnabled;
    }

    void debug(final String message, final Object... args) {
        if (debugEnabled) {
            write(logLineBuilderProvider.apply("DEBUG").apply(message, args));
        }
    }

    void info(final String message, final Object... args) {
        write(logLineBuilderProvider.apply("INFO").apply(message, args));
    }

    void warn(final String message, final Object... args) {
        write(logLineBuilderProvider.apply("WARN").apply(message, args));
    }

    void error(final String message, final Object... args) {
        write(logLineBuilderProvider.apply("ERROR").apply(message, args));
    }

    private void write(final String message) {
        System.out.println(message);
    }

    private String replacePlaceholders(final String message, final Object... args) {
        String filledMessage = message;
        for (final Object arg : args) {
            filledMessage = filledMessage.replaceFirst("\\{}", arg == null ? "null" : arg.toString());
        }
        return filledMessage;
    }
}
