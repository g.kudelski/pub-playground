import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * CSV Importer challenge program
 * <p>
 * Imports CSV files specified as parameters, from provided directories
 * or from current directory if no parameter given.
 * Target table named as source file.
 * Sensible configuration defaults applied:
 * - local Derby DB
 * - semicolon as delimiter
 * - 10 data rows per single INSERT to visualize accumulation for performance optimization
 * Custom configuration applied by relevant system environment variables (CSV_IMP_DB_URI,
 * CSV_IMP_DELIM, CSV_IMP_ROW_COUNT respectively.
 */
public class CsvImporter {

    interface ConfigurationDefaults {
        String DELIMITER = ";";
        String DB_CONNECTION_URL = "jdbc:derby:csv-import-sample-db;create=true";
        String SINGLE_INSERT_ROW_COUNT = "10";
    }

    interface ConfigurationEnvVars {
        String DELIMITER = ENV_VAR_PREFIX + "DELIM";
        String DB_URI = ENV_VAR_PREFIX + "DB_URI";
        String ROW_COUNT = ENV_VAR_PREFIX + "ROW_COUNT";
    }

    private static final String ENV_VAR_PREFIX = "CSV_IMP_";
    private static final SimpleLoggerStub LOG = SimpleLoggerStub.getLogger(CsvImporter.class);
    private static final BinaryOperator<String> getSystemEnvVariableOrDefault = (v, d) -> {
        final String value = System.getenv(v);
        return value != null ? value : d;
    };

    private DbInstance dbInstance;
    private int insertStatementRowCount;
    private CsvParser csvParser;

    private CsvImporter(final String delimiter, final String dbConnectionUrl, final int insertStatementRowCount) {
        this.csvParser = new CsvParser(delimiter);
        this.dbInstance = new DbInstance(dbConnectionUrl);
        this.insertStatementRowCount = insertStatementRowCount;
    }

    private int importFiles(final Stream<Path> csvFiles) {
        final FileReader<AccumulatingStatementExecutor> fileReader =
                new FileReader<>(tblName -> new AccumulatingStatementExecutor(dbInstance, csvParser, tblName, insertStatementRowCount),
                        c -> c::processHeaderLine, c -> c::processDataLine);
        return fileReader.readFiles(csvFiles);
    }

    private static List<Path> getSourceFiles(final List<String> params) {
        final ArrayList<Path> results = new ArrayList<>();
        if (params.size() == 0) {
            results.addAll(getSourceFiles(Collections.singletonList("")));
        }
        params.forEach(inputUri -> {
            final Path input = Paths.get(inputUri);
            if (Files.notExists(input)) {
                LOG.warn("Specified file '{}' does not exist, ignoring", input.getFileName().toString());
            } else if (Files.isDirectory(input)) {
                try {
                    results.addAll(Files.walk(input)
                                        .filter(p -> p.toString().toLowerCase().endsWith(".csv"))
                                        .collect(Collectors.toList()));
                } catch (IOException ex) {
                    LOG.error("IO exception while searching for *.csv files [{}]", ex.getMessage());
                    throw new RuntimeException();
                }
            } else {
                results.add(input);
            }
        });
        return results;
    }

    private static List<String> checkDebugFlagReturningFiles(String[] args) {
        return Stream.of(args).filter(arg -> {
            if (arg.toLowerCase().equals("--debug")) {
                LOG.setDebugEnabled(true);
                return false;
            } else {
                return true;
            }
        }).collect(Collectors.toList());
    }

    /**
     * Main program entry point
     *
     * @param args program parameters - CSV file names, paths containing them and '--debug' flag
     */
    public static void main(final String[] args) {
        final List<String> files = checkDebugFlagReturningFiles(args);
        final String delimiter = getSystemEnvVariableOrDefault.apply(ConfigurationEnvVars.DELIMITER,
                ConfigurationDefaults.DELIMITER);
        final String dbConnUrl = getSystemEnvVariableOrDefault.apply(ConfigurationEnvVars.DB_URI,
                ConfigurationDefaults.DB_CONNECTION_URL);
        final String insertRowCountStr = getSystemEnvVariableOrDefault.apply(ConfigurationEnvVars.ROW_COUNT,
                ConfigurationDefaults.SINGLE_INSERT_ROW_COUNT);
        if (!insertRowCountStr.matches("^\\d{1,}$")) {
            LOG.error("Non natural value for {} system variable, exiting", ConfigurationEnvVars.ROW_COUNT);
            System.exit(1);
        }
        final List<Path> sourcesList = getSourceFiles(files);
        if (sourcesList.isEmpty()) {
            LOG.error("No input file found or specified, exiting");
            System.exit(1);
        }
        LOG.info("Importing following sources: {}", sourcesList.stream()
                                                               .map(Path::toString)
                                                               .collect(Collectors.joining(", ")));
        final CsvImporter csvImporter = new CsvImporter(delimiter, dbConnUrl, Integer.valueOf(insertRowCountStr));
        final int totalLinesCount = csvImporter.importFiles(sourcesList.stream());
        LOG.info("Finished importing all files, {} lines in total", totalLinesCount);
    }
}
