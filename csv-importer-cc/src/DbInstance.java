import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Simple stub as database engine is unknown, in real life DataSource and PreparedStatement should be used.
 * Existing table handling implemented for Derby and MySQL only.
 */
class DbInstance {
    private static final SimpleLoggerStub LOG = SimpleLoggerStub.getLogger(DbInstance.class);
    private String dbUrl;

    public DbInstance(final String dbUrl) {
        this.dbUrl = dbUrl;
    }

    int executeUpdate(final String updateStatement) {
        try (final Connection conn = DriverManager.getConnection(dbUrl);
             final Statement statement = conn.createStatement()) {
            return statement.executeUpdate(updateStatement);
        } catch (SQLException ex) {
            if (ex.getSQLState().equals("X0Y32") || ex.getSQLState().equals("42S01")) {
                LOG.warn("Table already exists - data will be appended");
                return -1;
            } else {
                LOG.error("Exception while running statement: {}", ex.getMessage());
                throw new RuntimeException("Failed to execute SQL statement");
            }
        }
    }
}
