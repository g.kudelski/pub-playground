import org.junit.Test;

import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CsvParserTest {
    static final String aa = "aA";
    static final String bb = "bB";
    static final String cc = "cC";
    static final String delim = ";";
    static final String tbl = "tbl";

    @Test
    public void tableNameMatches_CreateStatement() {
        // given a read line has valid content
        String line = aa;
        // when translated to statement
        CsvParser csvParser = new CsvParser(";");
        String stmt = csvParser.getCreateStatement(line, tbl);
        // then statement refers to provided table
        assertTrue(stmt.matches("CREATE TABLE " + tbl + " \\(.*\\)\\)"));
    }

    @Test
    public void variousDelimiters_CsvParser() {
        // given a various delimiters
        String delim1 = delim;
        String delim2 = ",";
        String delim3 = ":";
        String delim4 = " ";
        String line1 = aa + delim1 + bb;
        String line2 = aa + delim2 + bb;
        String line3 = aa + delim3 + bb;
        String line4 = aa + delim4 + bb;
        // when translated to statement
        CsvParser parser1 = new CsvParser(delim1);
        String stmt1 = parser1.getCreateStatement(line1, tbl);
        CsvParser parser2 = new CsvParser(delim2);
        String stmt2 = parser2.getCreateStatement(line2, tbl);
        CsvParser parser3 = new CsvParser(delim3);
        String stmt3 = parser3.getCreateStatement(line3, tbl);
        CsvParser parser4 = new CsvParser(delim4);
        String stmt4 = parser4.getCreateStatement(line4, tbl);
        // then statement should be split properly
        assertTrue(stmt1.matches("CREATE TABLE \\w+ \\(" + aa + ".*, " + bb + ".*\\)\\)"));
        assertTrue(stmt2.matches("CREATE TABLE \\w+ \\(" + aa + ".*, " + bb + ".*\\)\\)"));
        assertTrue(stmt3.matches("CREATE TABLE \\w+ \\(" + aa + ".*, " + bb + ".*\\)\\)"));
        assertTrue(stmt4.matches("CREATE TABLE \\w+ \\(" + aa + ".*, " + bb + ".*\\)\\)"));
    }

    @Test
    public void empty_CreateStatement() {
        // given a read line is empty
        String line = "";
        // when translated to statement
        CsvParser csvParser = new CsvParser(";");
        String stmt = csvParser.getCreateStatement(line, tbl);
        // then statement is also empty
        assertTrue(stmt.isEmpty());
    }

    @Test
    public void singleColumn_CreateStatement() {
        // given a line contains single field
        String line = aa;
        // when translated to statement
        CsvParser csvParser = new CsvParser(delim);
        String stmt = csvParser.getCreateStatement(line, tbl);
        // then statement contains only one column definition
        assertTrue(stmt.matches("CREATE TABLE \\w+ \\(" + aa + ".*\\)\\)"));
    }

    @Test
    public void allColumnsTranslatedOrdered_CreateStatement() {
        // given a line contains multiple fields
        String line1 = aa + delim + aa + bb + delim + aa + cc + delim + bb;
        String line2 = aa + delim + bb + delim + aa + cc + delim + bb + cc + delim + cc;
        // when translated to statement
        CsvParser csvParser = new CsvParser(delim);
        String stmt1 = csvParser.getCreateStatement(line1, tbl);
        String stmt2 = csvParser.getCreateStatement(line2, tbl);
        // then statement contains the same amount of column definitions in identical order
        assertTrue(stmt1.matches("CREATE TABLE \\w+ \\(" + aa + ".*, " + aa + bb + ".*, " + aa + cc + ".*, " + bb + ".*\\)\\)"));
        assertTrue(stmt2.matches("CREATE TABLE \\w+ \\(" + aa + ".*, " + bb + ".*, " + aa + cc + ".*, " + bb + cc + ".*, " + cc + ".*\\)\\)"));
    }

    @Test
    public void tableNameMatches_InsertStatement() {
        // given a read line has valid content
        String line = aa;
        // when translated to statement
        CsvParser csvParser = new CsvParser(";");
        String stmt = csvParser.getInsertStatement(Stream.of(line), tbl).get();
        // then statement refers to provided table
        assertTrue(stmt.matches("INSERT INTO " + tbl + " VALUES \\(.*\\)"));
    }

    @Test
    public void empty_InsertStatement() {
        // given a read line is empty
        String line = "";
        // when translated to statement
        CsvParser csvParser = new CsvParser(";");
        Optional stmt = csvParser.getInsertStatement(Stream.of(line), tbl);
        // then statement is also empty
        assertFalse(stmt.isPresent());
    }

    @Test
    public void singleQuotInValue_InsertStatement() {
        // given value contains single quote
        String line1 = aa + "'" + bb;
        String line2 = aa + "'" + "'" + cc;
        // when translated to statement
        CsvParser csvParser = new CsvParser(delim);
        String stmt = csvParser.getInsertStatement(Stream.of(line1, line2), tbl).get();
        // then is escaped with double single quote
        assertTrue(stmt.matches("INSERT INTO .*'" + aa + "''" + bb + "'.*'" + aa + "''''" + cc + "'.*"));
    }

    @Test
    public void multipleLineValues_InsertStatement() {
        // given line carries many values
        String line1 = aa + delim + bb;
        String line2 = aa + delim + cc + delim + bb;
        // when translated to statement
        CsvParser csvParser = new CsvParser(delim);
        String stmt1 = csvParser.getInsertStatement(Stream.of(line1), tbl).get();
        String stmt2 = csvParser.getInsertStatement(Stream.of(line2), tbl).get();
        // then is all values are present in statement
        assertTrue(stmt1.matches("INSERT INTO .*\\(('\\w+'(, )?){2}\\)"));
        assertTrue(stmt2.matches("INSERT INTO .*\\(('\\w+'(, )?){3}\\)"));
    }

    @Test
    public void multipleLines_InsertStatement() {
        // given multiple lines are provided
        String line1 = aa + delim + bb;
        String line2 = aa + "'" + bb;
        String line3 = aa + "'" + "'" + cc;
        String line4 = aa + delim + cc + delim + bb;
        // when translated to statement
        CsvParser csvParser = new CsvParser(delim);
        String stmt = csvParser.getInsertStatement(Stream.of(line1, line2, line3, line4), tbl).get();
        // then all lines have corresponding row
        assertTrue(stmt.matches("INSERT INTO .* VALUES (\\([^\\(\\)]+\\)(, )?){4}"));
    }
}