import java.util.Objects;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

/**
 * CSV to SQL parser
 * Assuming 'VARCHAR(255)' for all columns, in real life we could imagine another header row with type definition
 * or some logic for automatic column type selection, although I believe that would be beyond this exercise.
 * Also, as DB engine is not known, assuming that all identifiers are ANSI compliant and do not require quoting.
 */
class CsvParser {
    private static final String TBL_NAME_PLACEHOLDER = "{tblName}";
    private static final String CREATE_TABLE_PREFIX = "CREATE TABLE " + TBL_NAME_PLACEHOLDER + " (";
    private static final String CREATE_TABLE_SUFFIX = ")";
    private static final String COLUMN_TYPE = "VARCHAR(255)";
    private static final String INSERT_INTO_PREFIX = "INSERT INTO " + TBL_NAME_PLACEHOLDER + " VALUES ";
    private static final BinaryOperator<String> stringCommaJoiner = (p, n) -> p.concat(", ").concat(n);
    private static final Predicate<String> notEmptyString = ((Predicate<String>) String::isEmpty).negate();

    private final Function<String, Stream<String>> csvSplitter;

    CsvParser(final String delimiter) {
        this.csvSplitter = line -> Stream.of(line.split(delimiter));
    }

    /**
     * Generates 'CREATE TABLE' DDL statement
     *
     * @param headerLine line containing delimiter separated column names
     * @param tableName  name for created table
     * @return generated DDL statement
     */
    String getCreateStatement(final String headerLine, final String tableName) {
        final StringBuilder ddlStatement = new StringBuilder();
        csvSplitter.apply(headerLine)
                   .filter(notEmptyString)
                   .map(c -> c.trim() + " " + COLUMN_TYPE)
                   .reduce(stringCommaJoiner)
                   .ifPresent(cols -> ddlStatement.append(CREATE_TABLE_PREFIX.replace(TBL_NAME_PLACEHOLDER, tableName))
                                                  .append(cols)
                                                  .append(CREATE_TABLE_SUFFIX));
        return ddlStatement.toString();
    }

    /**
     * Generates 'INSERT INTO' DML statement
     *
     * @param dataLines data rows with delimiter separated field values
     * @param tableName target table name
     * @return generated DML statement if row contain any data
     */
    Optional<String> getInsertStatement(final Stream<String> dataLines, final String tableName) {
        final StringBuilder dmlStatement = new StringBuilder();
        final UnaryOperator<String> quoteEscapingSingleQuotes = s -> "'" + s.replace("'", "''") + "'";
        dataLines.filter(notEmptyString)
                 .map(l -> csvSplitter.apply(l)
                                      .map(quoteEscapingSingleQuotes)
                                      .reduce(stringCommaJoiner)
                                      .map(v -> "(" + v + ")")
                                      .orElse(null))
                 .filter(Objects::nonNull)
                 .reduce(stringCommaJoiner).ifPresent(rows -> {
            dmlStatement.append(INSERT_INTO_PREFIX.replace(TBL_NAME_PLACEHOLDER, tableName))
                        .append(rows);
        });
        return dmlStatement.length() > 0 ? Optional.of(dmlStatement.toString()) : Optional.empty();
    }
}
